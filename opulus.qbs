import qbs 1.0
import qbs.base 1.0

Project {
    name : "opulus"

    references : [
        "core/core.qbs",
        "gui/gui.qbs",
        "main/main.qbs"
    ]
}
